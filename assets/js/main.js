(function ($) {
    let Coffee = {
        http: {
            url: '',
            ip: '',
        },

        /**
         *
         */
        status: {
            ready: 1,
            brewing: 2,
            finished: 4,
            notResponding: 8,
        },

        /**
         * Fetches the status of the coffee machine.
         */
        getStatus: function () {
            // TODO: implement POST logic.
            // Just returning a dummy status for testing purposes
            return this.status.ready;
        },

        updateStatus: function (elementName) {
            let status = this.getStatus();
            $(elementName)
                .removeClass('status-1')
                .removeClass('status-2')
                .removeClass('status-4')
                .removeClass('status-8')
                .addClass('status-' + status);
        }
    };

    window.setInterval(Coffee.updateStatus('body'), 1000);
})(jQuery);