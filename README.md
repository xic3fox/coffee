<h1>Kaffeemaschinensteuerung via WebApp</h1>

_Entwickelt von Thomas und Christian Artmann_

**Anforderungen der App:** Es muss einem möglich sein, dass man <br/> 
via aufrufen der Webseiten Aplikation auf dem Handy, die Kaffeemaschine <br/>
ferngesteuert starten kann und den momentanen Status von dem Server <br/>
und der Kaffeemaschine einsehen kann. Zudem ist es möglich den Server <br/>
neuzustarten. All dies soll auch möglich sein, wenn man nicht im gleichem <br/>
Netzwerk ist, oder sogar im Mobilfunknetzwerk.

**Ressourcen des Projekts:** 
<li>
Raspberry Pi 3 B+ <br/>
</li>
<br/>

**Mögliche Ziele:** Eine Anwendung für jegliche Kaffeemaschinen zu erstellen.
                              